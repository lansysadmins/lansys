# Lansys [Alpha 1.2]

Lansys is an LAN information system developed by Magnus "yugge" Aspling, Emil "lime" Tylén and Alex "Cheynas" Söderberg for the LUDDLAN series of LAN parties. It features scheduling, tournament management, game matchmaking and many more features to come.

## Where we are at right now:

The project is currently in late alpha stadium. This means that there still are some things not implemented. Listed below. Current estimations are that the system will reach beta stadium in a few days. From which the core systems will be up and only enhancement based features will be implemented, all other resources will be spent on bug fixing, security fixes, etc.

### Changelog:

#### Alpha 1.2:
* Users do no longer have to sign in to view schedule.
* Fixed spelling errors in schedule.
* Fixed XSS issues related to markdown fields.
* Starting LFG now joins IRC with a nick devoid of illegal signs.
* A non-final version of the handling blanks in lfg is in place.
* Added showing of registered users to Tournament modal
* Added warnings of closed and changed tournaments to the modals
* Schedule can now be extracted as iCal 


#### Alpha 1.1: 	
* Hover popover help added to LFG modal.
* When LFG MaxPlayers is set to 0, it now shows inf. in the game list.
* Trying to unreg multiple tournaments at the same time does not drop your reglist.
* LFG game name is now restricted to 20 characters.
* Trying to delete an already deleted tournament does no longer drop all tournaments.
* Script insertions (XSS) does no longer work on LFG popup.

#### Alpha 1: 

* First langroup version up.

### What does work:
+ Changing Users Password
+ Promoting and demoting users to Admin status
+ Removing Users
+ Updating Frontpage, both news and errors listings
+ Adding, removing and cancleing/removing a tournament
+ Hosting and joining a LFG game

### What does not work:
+ Launching a LFG game
+ Finding out any information on why a game was cancled.

### Features planned
+ Getting a list of tournament players when a tournament is about to start.
+ Starting a tournament lobby from which one can easily keep track of the tournament progress.
+ A user information page containing a list of the tournaments that the user is registered for as well as functions to change the users own password.
+ Schedule listing should be interactive and give you the register for tournament modal. Sans registrating for tournament, maybe a link to the tournament registration page instead?


## What should you test:
Everything that's implemented! Try to tear it apart, inject bad input, basically tear it to pieces. And every single bug you find should be reported.

### How to report bugs:
From https://bitbucket.org/lansysadmins/lansys

1. Click Issues on the navigation row.
2. Click "Create Issue"
3. Write an detailed title and description. Outlining what the problem is and if possible how to recreate it.
4. Please do not assign the bug to any specific user, this will be done on our side when we have judged the bug.
5. Set kind to what kind of issue you have at hand, please refer to the chart below on which levels mean what.
6. Set Priority to the degree of seriousness your issue is, please refer to the chart below on which levels mean what.
7. If you have any attachments (if a graphical bug occured a screenshot is nice) pleace press choose files and upload it.
8. Hit create issue!

The issue will be logged and we will recieve an mail stating the issue. When the issue is resolved you will recieve a mail stating this.

### Issue Kind Chart
* bug   - An error, unintended from the purpose of the code. 
* enhancement - Not an error, but a change you feel would make the system better 
* proposal - A feature not yet implemented that you feel would make system better. 
* task - not used 

### Issue Priority Chart

* Trivial - This issue doesn't matter at all, but is still an error. A typo is a typical trivial error 
* Minor - An annoyance. The functionality does still work, but with a work around. Forms not sending on enter key press would be a minor bug. A game taking in to long names for stuff is also a minor bug.
* Major - A major bug stops the functionality of a feature. If you can't join a game if your named ☑ or something similar, this would be a major bug. 
* Critical - A critical bug crashes the server. If after you do the bug the server is not responding. This is a critical bug. 
* Blocker - A blocker bug is a bug which is of such severity that we need to drop whatever we are doing to rush to fix that issue. We have no current example of such an issue.

## Change frequency
We will try to update the lansys as often as possible to respond to your issues. We will how ever do this on the development builds. Once we feel we have gotten enought stuff to update the beta build we will do this as well of updating this readme and send a message to your bitbucket accounts confirming this.

## Finally!
Happy bug hunting!